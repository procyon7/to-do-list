Thanks to the great tutorial created by Mustafa Murat Coşkun on Udemy.
This to-do list adds tasks to the list. It checks equality between new task to be added and the existing ones. It prevents duplicates. There is filtration feature to search tasks. Also, they can be removed one by one or in a lump.
