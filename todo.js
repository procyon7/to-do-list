// Selecting all elements
const form = document.querySelector("#todo-form");
const todoInput = document.querySelector("#todo");
const todoList = document.querySelector(".list-group");
const firstCardBody = document.querySelectorAll(".card-body")[0]; //there is 2 card body
const secondCardBody = document.querySelectorAll(".card-body")[1];
const filter = document.querySelector("#filter");
const clearButton = document.querySelector("#clear-todos");

eventListeners();
// Adding submit functionality (add to-do)

function eventListeners(){ // assigning all event listeners
    form.addEventListener("submit", addTodo);
    document.addEventListener("DOMContentLoaded", loadAllTodosToUI);
    secondCardBody.addEventListener("click", deleteTodo);
    filter.addEventListener("keyup", filterTodos);
    clearButton.addEventListener("click", clearAllTodos);
}

function clearAllTodos(e){
    if (confirm("Are you sure to delete all tasks?")){
        //todoList.innerHTML = ""; This line removes the list but it is slower than while loop below
        while(todoList.firstElementChild != null){
            todoList.removeChild(todoList.firstElementChild);
        }
        localStorage.removeItem("todos");  //todos is the key for todo list in the storage
    }
}

function filterTodos(e){
    const filterValue = e.target.value.toLowerCase();
    const listItems = document.querySelectorAll(".list-group-item");

    listItems.forEach(function(listItem){
        const text = listItem.textContent.toLowerCase();
        if (text.indexOf(filterValue) === -1){  //filter value is not present in the list
            listItem.setAttribute("style", "display: none !important");   //adding an inline css style
        }
        else{
            listItem.setAttribute("style", "display : block");
        }
    });
} 

function deleteTodo(e){
    //there are two parent classes of remove button. Should delete upper parent to remove that element
    if(e.target.className === "fa fa-remove"){
        e.target.parentElement.parentElement.remove();
        deleteTodoFromStorage(e.target.parentElement.parentElement.textContent);
        showAlert("success", "Task is deleted successfully.");
    }
}

function deleteTodoFromStorage(deletetodo){
    let todos = getTodosFromStorage();
    todos.forEach(function(todo, index){
        if(todo === deletetodo){
            todos.splice(index, 1);  //splice deletes specified number of items after the index. In this case it removes 1 item after index.
        }
    })

    localStorage.setItem("todos", JSON.stringify(todos));
}

function loadAllTodosToUI(){   //loads all todos from storage to UI
    let todos = getTodosFromStorage();
    
    todos.forEach(function(todo){
        addTodoToUI(todo);
    });
}

function addTodo(e){   
    const newTodo = todoInput.value.trim(); //trim removes spaces at the beginning and the end of input
    let toAdd = newTodo.toLowerCase();
    const listItems = document.querySelectorAll(".list-group-item");
    if(newTodo === ""){
        showAlert("danger", "Please enter a task.");  //check bootstrap alert types. danger, success etc
    }
    else{
        if(listItems.length === 0){
            addTodoToUI(newTodo);
            addTodoToStorage(newTodo);
            showAlert("success", "To-do is added successfully.")
        }
        else{   
            let exist = 0;
            listItems.forEach(function(listItem){
                let text = listItem.textContent.toLowerCase();
                //removes spaces between all characters and checks if a current task and new task match
                //if there is a match, it does not add that task
                text = text.replace(/ +/g, "");
                toAdd = toAdd.replace(/ +/g, "");
                
                if (text.indexOf(toAdd) != -1){  //if todo is already in the list, it won't be added to the list
                    exist ++;
                }
            });
            
            if(exist === 0){
                addTodoToUI(newTodo);
                addTodoToStorage(newTodo);
                showAlert("success", "To-do is added successfully.");
            }
            else{
                showAlert("warning", "Task has already been added.");
            }
        }
    }
    e.preventDefault(); //to prevent that form returns the same page again
}

function getTodosFromStorage(){
    let todos;
    if(localStorage.getItem("todos") === null){
        todos = [];
    }
    else{
        todos = JSON.parse(localStorage.getItem("todos")); //Since localStorage keeps them as string, convert them into array
    }

    return todos;
}

function addTodoToStorage(newTodo){
    let todos = getTodosFromStorage();
    todos.push(newTodo);
    //todos array is converted to string to keep it in localStorage
    localStorage.setItem("todos", JSON.stringify(todos)); //todos: key   
}

function showAlert(alertType, message){
    const alert = document.createElement("div");
    alert.className = `alert alert-${alertType}`; //used template literal to behave differently to alert types
    alert.textContent = message;
    //You didn't write this alert class inside card-body in index.html
    //You inserted alert element dynamically after checking some conditions.
    firstCardBody.appendChild(alert);

    //set timeout to remove alert after a short time
    setTimeout(function(){
        alert.remove();
    }, 1000); //1000 miliseconds, 1 second
}

function addTodoToUI(newTodo){ //adds this todo to list (to ul item)
    const listItem = document.createElement("li");  //ceating list item
    const link = document.createElement("a");  //creating link
    link.href = "#";
    link.className = "delete-item";
    link.innerHTML = "<i class = 'fa fa-remove'></i>";

    listItem.className = "list-group-item d-flex justify-content-between";
    listItem.appendChild(document.createTextNode(newTodo)); //creates a text node
    listItem.appendChild(link);

    //listItem will be added to ul (To-do list)
    todoList.appendChild(listItem);

    todoInput.value = "";  //clearing input value to see default placeholder
}